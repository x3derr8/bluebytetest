<?php namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Log;

/**
 * Class VerifyApiToken
 * @package App\Http\Middleware
 *
 * custom middleware that checks if current request uses a valid authentication header
 * this is just for a demonstration purposes, in real usages we would never save api token
 * in a config file, and each user should have different key, and we would check for other things like origins,
 * ssl...
 */
class VerifyApiToken {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		Log::info("verifying API token");

		if($auth = $request->header('Authentication') == env('API_TOKEN'))
		{
			Log::info("token verified");

			return $next($request);
		}

		Log::error("token is wrong: ".$request->header('Authentication'));

		return response('Unauthorized Access', 401);

	}

}
