<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// define route for our services
// we only have one service exposed
// and make sure to restrict username to alphanumerics
Route::get('services/user/{username}', 'ServiceController@getUser')->where(['username' => '[a-zA-Z0-9_]+']);

// leave at least index page
Route::get('/', 'WelcomeController@index');


