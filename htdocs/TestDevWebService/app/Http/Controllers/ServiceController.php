<?php namespace App\Http\Controllers;

use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Class ServiceController
 * @package App\Http\Controllers
 *
 * main controller for this project
 * it encapsulates logic for fetching data for specific user and returning it as json
 */
class ServiceController extends Controller {

	/**
	 * use middleware to check for valid requests
	 */
	public function __construct()
	{
		Log::info("New request");

		$this->middleware('apiToken');
	}

	/**
	 * get user data defined by username
	 *
	 * @param $username
	 */
	public function getUser($username)
	{
		Log::info("querying for user ".$username);

		// first get the user
		$user = User::where('username', '=', $username)->first();

		if(!$user)
		{
			// if user not found

			Log::error("user not found: ".$username);
			return response('Content not found', 404);
		}

		// if user found
		// then use eager loading to get the items and properties
		$user->items = $user->items()->with('properties')->get()->toArray();

		Log::info("user found");

		// now output result as json
		echo $user->toJson();
	}

}
