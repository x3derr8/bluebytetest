<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Item
 * @package App
 *
 * Item model
 */
class Item extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'item';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['idUser', 'name', 'game', 'expirationDate', 'quantity'];

    /**
     * don't use timestamps
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * one to many relation to properties
     */
    public function properties()
    {
        return $this->hasMany('\App\Property', 'idItem', 'idItem');
    }

}
