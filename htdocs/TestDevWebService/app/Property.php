<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Property
 * @package App
 *
 * Property model
 */
class Property extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'property';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['idItem', 'name', 'value'];

    /**
     * don't use timestamps
     *
     * @var bool
     */
    public $timestamps = false;

}
