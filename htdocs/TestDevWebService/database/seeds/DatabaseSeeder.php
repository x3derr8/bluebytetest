<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('UserTableSeeder');
		$this->call('ItemTableSeeder');
		$this->call('PropertyTableSeeder');
	}

}

/**
 * Class UserTableSeeder
 *
 * seed user data
 */
class UserTableSeeder extends Seeder {

	public function run()
	{
		DB::table('user')->delete();

		\App\User::create(array('username' => 'cecilia'));
		\App\User::create(array('username' => 'ana'));
	}

}

/**
 * Class ItemTableSeeder
 *
 * seed items data
 */
class ItemTableSeeder extends Seeder {

	public function run()
	{
		DB::table('item')->delete();

		\App\Item::create(array(
			'idUser' 			=> '1',
			'name' 				=> 'item1',
			'game' 				=> 'game1',
			'expirationDate' 	=> '2012-08-12',
			'quantity' 			=> '1'
		));

		\App\Item::create(array(
			'idUser' 			=> '1',
			'name' 				=> 'item2',
			'game' 				=> 'game2',
			'expirationDate' 	=> '2012-08-29',
			'quantity' 			=> '2'
		));

		\App\Item::create(array(
			'idUser' 			=> '2',
			'name' 				=> 'item3',
			'game' 				=> 'game1',
			'expirationDate' 	=> '2012-08-20',
			'quantity' 			=> '3'
		));
	}

}

/**
 * Class PropertyTableSeeder
 *
 * seed properties data
 */
class PropertyTableSeeder extends Seeder {

	public function run()
	{
		DB::table('property')->delete();

		\App\Property::create(array(
			'idItem'=> '1',
			'name' 	=> 'name1',
			'value' => 'value1'
		));

		\App\Property::create(array(
			'idItem'=> '1',
			'name' 	=> 'name2',
			'value' => 'value2'
		));

		\App\Property::create(array(
			'idItem'=> '2',
			'name' 	=> 'name3',
			'value' => 'value3'
		));

	}

}

