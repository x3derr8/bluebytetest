<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('property', function(Blueprint $table)
		{
			$table->increments('idProperty');
			$table->integer('idItem')->unsigned();
			$table->string('name', 45);
			$table->string('value', 45);

			// add foreign key to user id
			$table->foreign('idItem')->references('idItem')->on('item');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('property');
	}

}
