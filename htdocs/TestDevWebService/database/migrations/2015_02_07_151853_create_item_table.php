<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('item', function(Blueprint $table)
		{
			$table->increments('idItem');
			$table->integer('idUser')->unsigned();
			$table->string('name', 45);
			$table->string('game', 45);
			$table->date('expirationDate');
			$table->integer('quantity')->default(0);

			// add foreign key to user id
			$table->foreign('idUser')->references('idUser')->on('user');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('item');
	}

}
