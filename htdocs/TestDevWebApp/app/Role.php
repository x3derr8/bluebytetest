<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 * @package App
 *
 * Laravel model based on Role table
 */
class Role extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'role';

    /**
     * define custom id
     *
     * @var string
     */
    protected $primaryKey = 'idRole';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['roleDescription'];

    /**
     * don't use timestamps
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * define different role types
     *  - Admin: can see other users
     *  - Guest: regular user who can see only his items
     */
    const ROLE_ADMIN = 'Admin';
    const ROLE_GUEST = 'Guest';

}
