<?php namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

/**
 * Class AuthController
 * @package App\Http\Controllers\Auth
 *
 * controller that handles user login and logout
 * based on Laravel default AuthController
 */
class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|--------------------------------------------------------------------------
	| - this may be true, but we will use only login / logout functionalities
	| and ignore others
	| - access to methods is defined in routes.php
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * set redirection after login
	 *
	 * @var string
	 */
	public $redirectTo = '/';

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => 'getLogout']);
	}

	/**
	 * Handle a login request to the application.
	 *
	 * we will override this method because we are using slightly different User model
	 * then the one shipped with Laravel
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postLogin(Request $request)
	{
		$this->validate($request, [
			'username' => 'required',
			'password' => 'required',
		]);

		//------------------------------------------
		// normally we would do something like this
		//

		/*
		$credentials = $request->only('username', 'password');

		if ($this->auth->attempt($credentials, $request->has('remember')))
		{
			return redirect()->intended($this->redirectPath());
		}

		return redirect($this->loginPath())
			->withInput($request->only('username'))
			->withErrors([
				'username' => 'These credentials do not match our records.',
			]);
		*/

		// but instead we will simplify things a bit, as requested by specification
		//------------------------------------------

		$username = $request->input('username');
		$password = $request->input('password');

		$user = User::where('username', '=', $username)
			->where('password', '=', $password)
			->first();

		if($user)
		{
			// User is found, let's sign him in
			\Auth::loginUsingId($user->idUser);

			// and redirect him to a predefined path
			return redirect()->intended($this->redirectPath());
		}

		// if user not found, lets show him login form one more time
		return redirect($this->loginPath())
			->withInput($request->only('username'))
			->withErrors([
				'username' => 'These credentials do not match our records.',
			]);

	}

}
