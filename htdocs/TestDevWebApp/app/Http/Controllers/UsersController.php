<?php namespace App\Http\Controllers;

use App\Role;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Class UsersController
 * @package App\Http\Controllers
 *
 * This controller encapsulates the main logic of this project
 * - it is only accessible for authenticated users
 * - for admin users it displays list of all users and can get the info about items for each user using restful api
 * - for other users it only displays items for currently authenticated user
 */
class UsersController extends Controller {

    /**
     * define path to user service (restful api method)
     */
    const SERVICE_USER = 'services/user';

    /**
     * attach middleware to prevent unauthorised access
     */
	public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * this is a home page of this project
     * we will display a list of users (or only currently logged in user)
     * and by clicking on a specific user you will see his details
     *
     * please notice that we will get here only if we are signed in
     * (middleware is protecting us)
     */
    public function index()
    {
        // firstly we need to check the rule description for a current user
        if (\Auth::user()->role->roleDescription == Role::ROLE_ADMIN)
        {
            // if admin return all users
            return view('users.adminList')->with("users", User::all());
        }
        else if (\Auth::user()->role->roleDescription == Role::ROLE_GUEST)
        {
            // if guest display only his data
            return view('users.guestList')->with('user', \Auth::user());
        }
        else
        {
            // there is no third option, display error
            return view('users.notFound');
        }
    }

    /**
     * this is a method where we will fetch details for selected user
     * from our service and display it
     *
     * please notice that we will get here only if we are signed in
     * (middleware is protecting us)
     *
     * @param $username
     */
    public function getUser($username)
    {
        Log::info("fetching data for user: ".$username.", request done by ".\Auth::user()->username);

        // try to fetch requested user, check if it's in our db
        $user = User::where('username', '=', $username)->first();

        if(!$user)
        {
            Log::error("user ".$username." not found");

            // user not found
            return redirect('/')->with([
                'flash_message' => "User data wasn't found"
            ]);
        }
        else
        {
            // we will check again if current user has permissions to see requested user
            if(\Auth::user()->role->roleDescription == Role::ROLE_GUEST && $username != \Auth::user()->username)
            {
                Log::error("insufficient permissions: user ".\Auth::user()->username." is trying to get data for user ".$username);

                // user is fishing
                // throw him an error
                return redirect('/')->with([
                    'flash_message' => "You don't have permissions to see details for ".$username,
                    'flash_important' => true
                ]);
            }

            // otherwise we are fine
            // get the details
            $curl = new \anlutro\cURL\cURL;

            // make sure to send correct API TOKEN otherwise we will get 401
            $response = $curl->newrequest('get', env('API_URL') . self::SERVICE_USER . '/' . $username)
                ->setHeader('content-type', 'application/json')
                ->setHeader('Authentication', env('API_TOKEN'))
                ->send();

            if($response->statusCode == "200")
            {
                // we have all the data
                // just display them

                Log::info("user data found");

                $user = json_decode($response->body, true);
                return view('users.details', compact("user"));
            }
            else
            {
                Log::error("server responded with error: ".$response->statusCode." ".$response->body);

                // something went wrong
                // we need to display the error message
                return redirect('/')->with([
                    'flash_message' => "There was an error while fetching data, please try again in few minutes",
                    'flash_additional' => $response->body
                ]);

            }

        }

    }

}
