<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Authentication Routes
|--------------------------------------------------------------------------
|
| we will be using only login / logout functionality provided from Laravel
|
*/

Route::get('/auth/login', 'Auth\AuthController@getLogin');
Route::post('/auth/login', 'Auth\AuthController@postLogin');
Route::get('/auth/logout', 'Auth\AuthController@getLogout');


/*
|--------------------------------------------------------------------------
| Users Controller Routes
|--------------------------------------------------------------------------
|
| we have only two, one for accessing the list of users and second one for
| getting the details of specific user
|
*/
Route::get('/user/{username}', 'UsersController@getUser')->where('username', '[a-zA-Z0-9_]+');;
Route::get('/', 'UsersController@index');
