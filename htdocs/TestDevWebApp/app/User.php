<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * Class User
 * @package App
 *
 * User model based on user table
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user';

	/**
	 * define custom id
	 *
	 * @var string
	 */
	protected $primaryKey = 'idUser';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['username', 'idRole'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password'];

	/**
	 * don't use timestamps
	 *
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * provides database relation to role model
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function role()
	{
		return $this->hasOne('\App\Role', 'idRole', 'idRole');
	}

	/*
	|--------------------------------------------------------------------------
	| Hide rememeber me token
	|--------------------------------------------------------------------------
	|
	| Laravel is forcing us to use remember me token in user model, so this
	| column should exists in user table
	|
	| Because in our example we won't use that, we will simply override this
	| functionality
	|
	*/

	public function getRememberToken()
	{
		return null; // not supported
	}

	public function setRememberToken($value)
	{
		// not supported
	}

	public function getRememberTokenName()
	{
		return null; // not supported
	}

	/**
	 * Overrides the method to ignore the remember token.
	 */
	public function setAttribute($key, $value)
	{
		$isRememberTokenAttribute = $key == $this->getRememberTokenName();
		if (!$isRememberTokenAttribute)
		{
			parent::setAttribute($key, $value);
		}
	}

}
