$(document).ready(function() {

    // add delay to flash messages
    $('div.alert').not('.alert-info').delay(3000).slideUp();

})