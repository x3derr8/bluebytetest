<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('RoleTableSeeder');
		$this->call('UserTableSeeder');
	}

}

/**
 * Class RoleTableSeeder
 *
 * seed role data
 */
class RoleTableSeeder extends Seeder {

	public function run()
	{
		DB::table('role')->delete();

		\App\Role::create(array('roleDescription' => 'Admin'));
		\App\Role::create(array('roleDescription' => 'Guest'));
	}

}

/**
 * Class UserTableSeeder
 *
 * seed user data
 */
class UserTableSeeder extends Seeder {

	public function run()
	{
		DB::table('user')->delete();

		\App\User::create(array(
			'username' => 'cecilia',
			'password' => 'cecilia1',
			'idRole' => 1
		));
		\App\User::create(array(
			'username' => 'ana',
			'password' => 'ana1',
			'idRole' => 2
		));
	}

}