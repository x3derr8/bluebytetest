@extends('app')

@section('content')
    <p>Sorry, but your data couldn't be found. Please contact us for more details.</p>
@endsection