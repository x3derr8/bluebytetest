@extends('app')

@section('content')
    <table class="table">
        <thead>
            <tr>
                <td>Id</td>
                <td>Username</td>
                <td>Role</td>
                <td>Options</td>
            </tr>
        </thead>

        <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>{{ $user->idUser }}</td>
                    <td>{{ $user->username }}</td>
                    <td>{{ $user->role->roleDescription }}</td>
                    <td><a href="{{{ url("/user", $user->username) }}}">View Details</a></td>
                </tr>
            @endforeach
        </tbody>

    </table>
@endsection