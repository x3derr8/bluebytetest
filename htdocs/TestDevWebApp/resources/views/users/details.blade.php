@extends('app')

@section('content')
    <h1>List of items for user <strong>{{ $user['username'] }}</strong></h1>
    <ul>
        @foreach($user['items'] as $item)
            <li>
                {{ $item['name'] }}
                <ul>
                    <li>Game: {{ $item['game'] }}</li>
                    <li>Quantity: {{ $item['quantity'] }}</li>
                    <li>Expiration Date: {{ $item['expirationDate'] }}</li>
                    @if (count($item['properties']))
                        <li>Properties:
                            <ul>
                                @foreach($item['properties'] as $property)
                                    <li>{{ $property['name'] }}: {{ $property['value'] }}</li>
                                @endforeach
                            </ul>
                        </li>
                    @endif
                </ul>
            </li>
        @endforeach
    </ul>
@endsection