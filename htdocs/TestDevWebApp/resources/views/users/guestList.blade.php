@extends('app')

@section('content')
    <table class="table">
        <thead>
        <tr>
            <td>Username</td>
            <td>Options</td>
        </tr>
        </thead>

        <tbody>
            <tr>
                <td>{{ $user->username }}</td>
                <td><a href="{{{ url("/user", $user->username) }}}">View Details</a></td>
            </tr>
        </tbody>

    </table>
@endsection